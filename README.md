# Mp3Filesort

### Запуск 
```
python3 main.py
```

Основное приложения и два модуля, один - работа с ОС, другой - ID3 теги

### Параметры
```
usage: main.py [-h] [--src-dir SRC_DIR] [--dst-dir DST_DIR]

Read MP3 tags and sort files

optional arguments:
  -h, --help            show this help message and exit
  --src-dir SRC_DIR, -s SRC_DIR
                        Source directory
  --dst-dir DST_DIR, -d DST_DIR
                        Destination directory
```

### Что обрабатывается
- Случай с полныи отсутствием тегов у трека
- Отсутствие song (title) у трека
- Отстутствие входной директории
- Нехватка прав чтения входной директории
- Отсутствие выходной директории
- Нехватка прав записи выходной директории
- Отсутствие прав на какой-либо определенный файл
- Конфликт (Азат сказал заменять, поэтому destination файл удаляется и заменяется новым)
- Отсутсвие файлов
- Параметры командной строки, вход, выход, help
- Разные сепараторы, а именно . // \
- Протестировал и под Windows, и Ubuntu WSL

### Баги
- You tell me
- ~~Я не уверен, что это баг реализации, но мой редактор Mp3Tag замусоривает теги виесто их удаления из-за чего возможно создание файлов типа ЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄ - ЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄ - Є
ЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄЄ.mp3, я отловил часть таких ситуации, но не могу быть уверен, что он не проявится снова~~


#### Пример вывода
```
>> python3 main.py -s input -d test                                                                                                                ✔  at 02:06:14  9% ▁
Destination directory test not found
Creating output directory
input/muzlome_Dead_Or_Alive_-_You_Spin_Me_Round_Like_a_Record_48283438.mp3 -> test/Dead Or Alive/Pure... Fitness/You Spin Me Round (Like a Record) - Dead Or Alive - Pure... Fitness.mp3;
input/muzlome_Elvis_Presley_JXL_-_A_Little_Less_Conversation_47855947 (1).mp3 -> test/Elvis Presley, JXL/Pure... Pop/A Little Less Conversation - Elvis Presley, JXL - Pure... Pop.mp3;
File test/Elvis Presley, JXL/Pure... Pop/A Little Less Conversation - Elvis Presley, JXL - Pure... Pop.mp3 already exists, replacing
input/muzlome_Elvis_Presley_JXL_-_A_Little_Less_Conversation_47855947.mp3 -> test/Elvis Presley, JXL/Pure... Pop/A Little Less Conversation - Elvis Presley, JXL - Pure... Pop.mp3;
input/muzlome_Imagine_Dragons_-_Believer_47828250.mp3 -> test/Imagine Dragons/Evolve/Believer - Imagine Dragons - Evolve.mp3;
Skipping input/muzlome_Imagine_Dragons_-_Thunder_47828258.mp3, no tags found
input/muzlome_Imagine_Dragons_-_Warriors_47829305.mp3 -> test/Imagine Dragons/Smoke + Mirrors/Warriors - Imagine Dragons - Smoke + Mirrors.mp3;
input/muzlome_Linkin_Park_-_Faint_47828666.mp3 -> test/Linkin Park/Meteora/Faint - Linkin Park - Meteora.mp3;
input/muzlome_Modern_Talking_-_Brother_Louie_47835499.mp3 -> test/Modern Talking/Original Album Classics/muzlome_Modern_Talking_-_Brother_Louie_47835499.mp3;
Skipping input/muzlome_Queen_-_We_Will_Rock_You_47828511.mp3, no tags found
Can't access file input/muzlome_Rammstein_-_AUSLNDER_64285337.mp3 insufficient permissions
input/muzlome_Rammstein_-_Deutschland_63121880.mp3 -> test/Rammstein/Deutschland/Deutschland - Rammstein - Deutschland.mp3;
input/muzlome_Rammstein_-_Du_Hast_63121920.mp3 -> test/Rammstein/Sehnsucht/Du Hast - Rammstein - Sehnsucht.mp3;
input/muzlome_Rammstein_-_Feuer_frei_57658984.mp3 -> test/Rammstein/Mutter/Feuer frei! - Rammstein - Mutter.mp3;
input/muzlome_Rammstein_-_Ich_will_57658983.mp3 -> test/Rammstein/Mutter/Ich will - Rammstein - Mutter.mp3;
input/muzlome_Rammstein_-_RADIO_63782865 (1).mp3 -> test/Rammstein/RADIO/RADIO - Rammstein - RADIO.mp3;
File test/Rammstein/RADIO/RADIO - Rammstein - RADIO.mp3 already exists, replacing
input/muzlome_Rammstein_-_RADIO_63782865.mp3 -> test/Rammstein/RADIO/RADIO - Rammstein - RADIO.mp3;
input/muzlome_Scorpions_-_Still_Loving_You_47954789.mp3 -> test/Scorpions/Love At First Sting/Still Loving You - Scorpions - Love At First Sting.mp3;
Can't access file input/muzlome_Skillet_-_Monster_47950047.mp3 insufficient permissions
Done.
```

#### Дерево каталогов

![](https://i.imgur.com/A7TbQtg.png)