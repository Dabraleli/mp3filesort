import argparse
import os

from OSUtils import OSUtils
from ID3Tag import ID3Tag


def check_source_directory(path):
    src_probe = OSUtils.access_directory(path, os.R_OK)  # можем ли читать из директории
    if src_probe != OSUtils.Error.OK:
        if src_probe == OSUtils.Error.Permission:
            print(f'Insufficient permissions on source directory {path}')
            quit(1)
        if src_probe == OSUtils.Error.NotFound:
            print(f'Source directory {path} not found')
            quit(2)


def check_destination_directory(path):
    dst_probe = OSUtils.access_directory(path, os.W_OK)  # можем ли писать в дерикторию
    if dst_probe != OSUtils.Error.OK:
        if dst_probe == OSUtils.Error.Permission:
            print(f'Insufficient permissions on destination directory {path}')
            quit(3)
        if dst_probe == OSUtils.Error.NotFound:
            print(f'Destination directory {path} not found')
            print('Creating output directory')
            if OSUtils.create_path(path) != OSUtils.Error.OK:
                quit(4)


def handle_file(src_dir, filename, dst_dir):
    src_file_path = os.path.join(src_dir, filename)
    mp3tag = ID3Tag(src_file_path)
    if OSUtils.access_file(src_file_path, os.R_OK) == OSUtils.Error.Permission or not mp3tag.is_valid():  # можем ли читать файл
        print(f'Can\'t access file {src_file_path} insufficient permissions')
        return

    tag_info = mp3tag.get_tags()  # теги mp3

    is_file_valid = tag_info.artist is not None and tag_info.album is not None  # подходит ли нам файл
    if is_file_valid:
        new_file_name = f'{tag_info.song} - {tag_info.artist} - {tag_info.album}.mp3' if tag_info.song is not None \
            else file  # если названия нет
        new_file_path = os.path.join(dst_dir, tag_info.artist, tag_info.album, new_file_name)
        # создание пути
        if OSUtils.create_path(os.path.join(dst_dir, tag_info.artist, tag_info.album)) != OSUtils.Error.OK:
            return

        if os.path.exists(new_file_path) or os.path.exists(new_file_path):
            print(f'File {new_file_path} already exists, replacing')
            os.remove(new_file_path)

        print(src_file_path, '->', new_file_path + ';')  # не знаю зачем ; но так в задаче
        os.rename(src_file_path,
                  new_file_path)  # все проверки пройдены, можно переносить
    else:  # если в тегах нет информации
        print(f'Skipping {src_file_path}, no tags found')


if __name__ == '__main__':
    # аргументы командной строки
    parser = argparse.ArgumentParser(description='Read MP3 tags and sort files')
    parser.add_argument('--src-dir', '-s', type=str,
                        help='Source directory', default='.')
    parser.add_argument('--dst-dir', '-d', type=str,
                        help='Destination directory', default='.')
    args = parser.parse_args()

    # весь этот блок нужен для нормальной обработки любых путей
    # примеры ./output .\\debug ./long\\path/with\\wrong/seperators output
    if not os.path.isabs(args.src_dir) and args.src_dir != '.':
        args.src_dir = args.src_dir.strip('.\\/')
        # убираем лишние . / и \\
    if not os.path.isabs(args.dst_dir) and args.dst_dir != '.':
        args.dst_dir = args.dst_dir.strip('.\\/')

    args.src_dir = os.path.normpath(args.src_dir)  # нормалищация сепараторов
    args.dst_dir = os.path.normpath(args.dst_dir)

    check_source_directory(args.src_dir)
    check_destination_directory(args.dst_dir)

    filenames = OSUtils.list_files_in_directory(args.src_dir, 'mp3')  # поиск всех mp3
    for file in filenames:
        handle_file(args.src_dir, file, args.dst_dir)

    print('Done.')
