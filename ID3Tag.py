# модуль для работы с id3 тэгами
from mp3_tagger import MP3File, VERSION_2
from collections import namedtuple


class ID3Tag:
    def __init__(self, path):
        try:
            self._file = MP3File(path)
            self._file.set_version(VERSION_2)
        except OSError:  # фикс для windows, где нет нормальногой обработки пермов
            self._is_valid = False
        else:
            self._is_valid = True

    def is_valid(self):
        return self._is_valid

    # получения списка тегов
    def get_tags(self):
        Tag = namedtuple('Tag', 'artist, song, album')

        # по условию возвращаются только artist, song и album, но допилить для остальных тегов тоже реально
        return Tag(artist=self._get_better_tag(self._file.artist),
                   song=self._get_better_tag(self._file.song),
                   album=self._get_better_tag(self._file.album))

    @staticmethod
    def _get_better_tag(tag):
        if tag and tag is not None:
            return ID3Tag._verify_tag_for_error(tag.rstrip('\x00').strip())
        return None

    @staticmethod
    def _verify_tag_for_error(name):
        if name.count(name[0]) == len(name):
            return None
        else:
            return name

