# модуль для безопасной работы с ОС
from enum import Enum
import os


class OSUtils:
    class Error(Enum):
        OK = 0
        Permission = 1
        NotFound = 2
        CreateError = 3

    # обёртка для более удобной фильтрации только mp3 файлов
    @staticmethod
    def list_files_in_directory(path, ext=''):
        return [f for f in os.listdir(path) if os.path.isfile(os.path.join(path, f)) and (ext == '' or f.endswith(ext))]

    @staticmethod
    def create_path(path):
        try:
            os.makedirs(path, mode=0o777, exist_ok=True)
            return OSUtils.Error.OK
        except OSError as e:
            # тут можно поспорить о правильности принта в модуле, но я считаю, что такой повод является достаточным
            print(e)
            return OSUtils.Error.CreateError

    # проверка И на существование И на права
    @staticmethod
    def access_directory(path, access_right):
        if os.path.exists(path) and os.path.isdir(path):
            if not os.access(path, access_right):
                return OSUtils.Error.Permission
            else:
                return OSUtils.Error.OK
        return OSUtils.Error.NotFound

    # проверка на права, на существование не нужно, проверка снаружи
    @staticmethod
    def access_file(path, access_right):
        if not os.access(path, access_right):
            return OSUtils.Error.Permission
        else:
            return OSUtils.Error.OK
